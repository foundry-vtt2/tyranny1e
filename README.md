# tyranny1e

This is the Tyranny Game System for use with Foundry Virtual Table Top

This system uses trademarks and/or copyrights owned by Paizo Inc., which are used under Paizo's Community Use Policy. We are expressly prohibited from charging you to use or access this content. This system is not published, endorsed, or specifically approved by Paizo Inc. For more information about Paizo's Community Use Policy, please visit paizo.com/communityuse. For more information about Paizo Inc. and Paizo products, please visit paizo.com.

## Licenses

**Project Licensing:**

- All HTML, CSS and Javascript in this project is licensed under the Apache License v2.

**Content Usage and Licensing:**

- Any Pathfinder Second Edition information used under the Paizo Inc. Community Use Policy (<https://paizo.com/community/communityuse>)
- Game system information and mechanics are licensed under the Open Game License (OPEN GAME LICENSE Version 1.0a).
- License information for the art used in this project is included in the ./packs/ folder alongside the JSON of where it is referenced.

**Virtual Table Top Platform Licenses:**

- Foundry VTT support is covered by the following license: [Limited License Agreement for module development 09/02/2020](https://foundryvtt.com/article/license/).

## Patch Notes

See [CHANGELOG.md](./CHANGELOG.md)

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md)
