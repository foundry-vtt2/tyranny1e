# Contributing

Thanks for your interest in my game system!

If you wish to contribute patches then send an email to my gmail account
_thebeardedmage636_
and request that I add you as a developer to the repo :-)
